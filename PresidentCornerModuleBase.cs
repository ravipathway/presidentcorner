﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Common;
using System.Text;
using DotNetNuke.Services.FileSystem;

namespace NRNA.Modules.PresidentCorner
{
    public class PresidentCornerModuleBase : PortalModuleBase
    {
        public int CornerId
        {
            get
            {
                var qs = Request.QueryString["pcid"];
                if (qs != null)
                    return Convert.ToInt32(qs);
                return -1;
            }
        }
        
        public int PageNumber
        {
            get
            {
                if (Request.QueryString["p"] == null)
                    return 1;
                else
                    return int.Parse(Request.QueryString["p"]);
            }
        }

        public int PageSize
        {
            get
            {
                if (Request.QueryString["psize"] == null)
                    return 5;
                else
                    return int.Parse(Request.QueryString["psize"]);
            }
        }

        public void BuildPageList(int totalItems, System.Web.UI.WebControls.Literal litPagingControl)
        {
            float numberOfPages = totalItems / (float)PageSize;
            int intNumberOfPages = Convert.ToInt32(numberOfPages);
            if (numberOfPages > intNumberOfPages)
            {
                intNumberOfPages++;
            }

            if (intNumberOfPages > 1)
            {
                litPagingControl.Text = "<ul class='pagination'>";
                if (PageNumber > 1)
                {
                    litPagingControl.Text += "<li><a href ='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + (PageNumber - 1).ToString() }) + "' class='prev'><span class='icon icon-arrow-left'></span></a></li>";
                }
                for (int p = 1; p <= intNumberOfPages; p++)
                {

                    if (p == PageNumber)
                    {
                        litPagingControl.Text += "<li><strong>" + p.ToString() + "</strong></li>";
                    }
                    else
                    {
                        litPagingControl.Text += "<li><a href='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + p.ToString() }) + "'>" + p.ToString() + "</a></li>";
                    }

                }
                if (PageNumber < intNumberOfPages)
                {
                    litPagingControl.Text += "<li><a href ='" + Globals.NavigateURL(this.TabId, string.Empty, new string[] { "p=" + (PageNumber + 1).ToString() }) + "' class='prev'><span class='icon icon-arrow-right'></span></a></li>";
                }
                litPagingControl.Text += "</ul>";
            }
        }

        public string GetImageFromImageID(string fileId)
        {
            if (fileId == "-1")
            {
                fileId = "3245";
            }
            StringBuilder sb = new StringBuilder("/Portals/");
            try
            {
                IFileInfo fi = FileManager.Instance.GetFile(Convert.ToInt32(fileId));
                sb.Append(fi.PortalId);
                sb.Append("/");
                sb.Append(fi.RelativePath);
            }
            catch (Exception ex)
            {
                return "";
            }
            return sb.ToString();
        }

        public string GetFormatedDate(string Startdatetime)
        { 
            return Convert.ToDateTime(Startdatetime).ToString("dd MMM yyyy");
        }
    }
}