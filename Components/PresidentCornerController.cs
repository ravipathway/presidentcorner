﻿/*
' Copyright (c) 2017 Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/
using System.Collections.Generic;
using DotNetNuke.Data;

namespace NRNA.Modules.PresidentCorner.Components
{
    class PresidentCornerController
    {
        public void CreateItem(PresidentCorner t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                rep.Insert(t);
            }
        }

        public void DeleteItem(int itemId, int moduleId)
        {
            var t = GetItem(itemId, moduleId);
            DeleteItem(t);
        }

        public void DeleteItem(PresidentCorner t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                rep.Delete(t);
            }
        }

        public IEnumerable<PresidentCorner> GetItems(int moduleId)
        {
            IEnumerable<PresidentCorner> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                t = rep.Get(moduleId);
            }
            return t;
        }

        public PresidentCorner GetItem(int itemId, int moduleId)
        {
            PresidentCorner t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                t = rep.GetById(itemId, moduleId);
            }
            return t;
        }

        public void DeletePresidentCorner(int itemId, int portalId)
        {
            var t = GetPresidentCorner(itemId, portalId);
            DeletePresidentCorner(t);
        }

        public void DeletePresidentCorner(PresidentCorner t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                rep.Delete(t);
            }
        }

        public void UpdateItem(PresidentCorner t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                rep.Update(t);
            }
        }

        public PresidentCorner GetPresidentCorner(int itemId, int portalId)
        {
            PresidentCorner t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                t = rep.GetById(itemId, portalId);
            }
            return t;
        }

        public IEnumerable<PresidentCorner> GetPresidentCorner()
        {
            IEnumerable<PresidentCorner> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<PresidentCorner>(System.Data.CommandType.Text, @"SELECT * FROM [NRNAPresidentCorner] ORDER BY Date DESC");
            }
            return t;
        }

        public IEnumerable<PresidentCorner> GetPresidentCorner(int portalId, int PageIndex, int PageSize)
        {
            IEnumerable<PresidentCorner> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<PresidentCorner>();
                t = rep.Find(PageIndex, PageSize, "WHERE PortalId= @0", portalId);
            }
            return t;
        }

        public int GetTotalItemCount(int portalId)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                return ctx.ExecuteScalar<int>(System.Data.CommandType.Text, "SELECT COUNT(CornerId) FROM [NRNAPresidentCorner] WHERE  PortalId = @0", portalId);
            }
        }

        public IEnumerable<PresidentCorner> GetTop3Corner(int portalId)
        {
            IEnumerable<PresidentCorner> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<PresidentCorner>(System.Data.CommandType.Text, @"SELECT TOP 3 * FROM [NRNAPresidentCorner] WHEre [PortalId]=@0 ORDER BY Date Desc",portalId);
            }
            return t;
        }
    }
}
