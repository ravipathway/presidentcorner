﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.UI.WebControls;
using NRNA.Modules.PresidentCorner.Components;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;

namespace NRNA.Modules.PresidentCorner
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from PresidentCornerModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                var controlToLoad = "";

                if (Settings.Contains("DefaultView"))
                {
                    var defaultview = Settings["DefaultView"].ToString();
                    switch (defaultview)
                    {
                        case "0":
                            if (CornerId <= 0)
                            {
                                controlToLoad = "Controls/PresidentCornerList.ascx"; break;
                            }
                            else
                            {
                                controlToLoad = "Controls/PresidentCornerView.ascx";break;
                            }
                        case "1": controlToLoad = "Controls/PresidentCornerListForHomePage.ascx"; break;
                        case "2": controlToLoad = "Controls/PresidentCornerListForEdit.ascx"; break;
                        case "3": controlToLoad = "Controls/Edit.ascx"; break;
                    }
                }
                else
                {
                    if (CornerId <= 0)
                    {
                        controlToLoad = "Controls/PresidentCornerList.ascx";
                    }
                    else
                    {
                        controlToLoad = "Controls/PresidentCornerView.ascx";
                    }
                }
                var mbl = (PresidentCornerModuleBase)LoadControl(controlToLoad);
                mbl.ModuleConfiguration = ModuleConfiguration;
                mbl.ID = System.IO.Path.GetFileNameWithoutExtension(controlToLoad);
                phViewControl.Controls.Add(mbl);

            }
            catch (Exception ex)
            {
                //throw;
            }
        }
    }
}