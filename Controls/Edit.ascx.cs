﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.PresidentCorner.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.PresidentCorner.Controls
{
    public partial class Edit : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Implement your edit logic for your module
                if (!Page.IsPostBack)
                {
                    //check if we have an ID passed in via a querystring parameter, if so, load that item to edit.
                    //ItemId is defined in the ItemModuleBase.cs file
                    if (CornerId > 0)
                    {
                        var tc = new PresidentCornerController();

                        var t = tc.GetPresidentCorner(CornerId, PortalId);
                        if (t != null)
                        {
                            txtTitle.Text = t.CornerTitle;
                            txtDate.SelectedDate = Convert.ToDateTime(t.Date.ToString());
                            fileuploader.FileID = Convert.ToInt32(t.Attachment);
                            txtDescription.Text = t.Description;
                        }
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {  
            var t = new NRNA.Modules.PresidentCorner.Components.PresidentCorner();
            var tc = new PresidentCornerController();

            if (CornerId > 0)
            {
                t = tc.GetPresidentCorner(CornerId, PortalId);
            }
            else
            {
                t.CreatedByUserId = UserId;
                t.CreatedOnDate = DateTime.Now;
            }

            t.CornerTitle = txtTitle.Text;
            t.Date = Convert.ToDateTime(txtDate.SelectedDate);
            t.Attachment = fileuploader.FileID.ToString();
            t.Description = txtDescription.Text;
            t.LastModifiedByUserId = UserId;
            t.LastModifiedOnDate = DateTime.Now;
            t.ModuleId = ModuleId;
            t.PortalId = PortalId;
            t.PostedFromDomain = PortalAlias.HTTPAlias;
            t.Country = PortalSettings.PortalName;
           
            if (CornerId > 0)
            {
                tc.UpdateItem(t);
            }
            else
            {
                tc.CreateItem(t);
            }
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }
    }
}