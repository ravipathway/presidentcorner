﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PresidentCornerListForEdit.ascx.cs" Inherits="NRNA.Modules.PresidentCorner.Controls.PresidentCornerListForEdit" %>
<h2>Manage President Corner</h2>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-blue" Text="Add President Corner" Visible="true" Enabled="true"
    NavigateUrl="~/add-president-corner" Target="_blank" />
<br />
<br />

<asp:GridView runat="server" ID="gv" AllowCustomPaging="False" AllowPaging="True" PageSize="10" DataKeyNames="CornerId"
    AutoGenerateColumns="False" OnRowDataBound="gv_RowDataBound" OnRowCommand="gv_RowCommand" OnRowDeleting="gv_RowDeleting"
    OnPageIndexChanging="gv_PageIndexChanging">
    <Columns>
        <asp:TemplateField HeaderText="SNo." ItemStyle-Width="5">
            <ItemTemplate>
                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" BackColor="transparent" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="CornerTitle" HeaderText="Title" ItemStyle-Width="420"></asp:BoundField>
        <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="50" />
        <asp:TemplateField HeaderText="Manage" ItemStyle-Width="170">
            <ItemTemplate>
                <asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-green" Text="Edit" Visible="true" Enabled="true"
                    Target="_blank" NavigateUrl='<%# "~/add-president-corner/pcid/" + DataBinder.Eval(Container.DataItem,"CornerId").ToString() %>' />
                
                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button  button-donate" Text="Delete" Visible="true" Enabled="true" CommandName="Delete" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
