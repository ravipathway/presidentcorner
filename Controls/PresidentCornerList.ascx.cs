﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.PresidentCorner.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.PresidentCorner.Controls
{
    public partial class PresidentCornerList : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int totalRecords = 0;
                var tc = new PresidentCornerController();
                rptItemList2.DataSource = tc.GetPresidentCorner(PortalId, PageNumber - 1, PageSize);
                totalRecords = tc.GetTotalItemCount(PortalId);
                rptItemList2.DataBind();
                BuildPageList(totalRecords, litPaging);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}