﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.PresidentCorner.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.PresidentCorner.Controls
{
    public partial class PresidentCornerView : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (CornerId > 0)
                    {
                        NRNA.Modules.PresidentCorner.Components.PresidentCorner curItem;
                        curItem = new PresidentCornerController().GetPresidentCorner(CornerId, PortalId);
                        //display Event info on the view control
                        if (curItem != null)
                        {
                            litTitle.Text = curItem.CornerTitle;
                            litDate.Text = GetFormatedDate(curItem.Date.ToShortDateString());
                            litSource.Text = curItem.Country;
                            litDesription.Text = Server.HtmlDecode(curItem.Description);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }
    }
}