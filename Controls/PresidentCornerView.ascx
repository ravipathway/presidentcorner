﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PresidentCornerView.ascx.cs" Inherits="NRNA.Modules.PresidentCorner.Controls.PresidentCornerView" %>
<%@ Register TagPrefix="dnn" Assembly="DotNetNuke.Web"
    Namespace="DotNetNuke.Web.UI.WebControls" %>

<h1>
    <asp:Literal ID="litTitle" runat="server" /></h1>
<span class="meta">Date:
        <asp:Literal runat="server" ID="litDate"></asp:Literal>
    | Source:
    <asp:Literal ID="litSource" runat="server"></asp:Literal>
</span>
<hr>
<asp:Literal ID="litDesription" runat="server" />