﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Edit.ascx.cs" Inherits="NRNA.Modules.PresidentCorner.Controls.Edit" %>
<%@ Register TagPrefix="dnn" TagName="label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx" %>
<%@ Register TagPrefix="dnn" TagName="FilePickerUploader" Src="~/controls/FilePickerUploader.ascx" %>
<%@ Register TagPrefix="dnnui" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<div class="dnnForm dnnEditBasicSettings" id="dnnEditBasicSettings">
    <h2>Add/Edit President Corner</h2>

    <table cellpadding="5">
        <tr>
            <td style="vertical-align: top; width: 200px;">Title</td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server" Width="400px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="txtTitle"
                    ErrorMessage="Title is required."
                    CssClass="NormalRed">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 150px;">Date</td>
            <td>
                <dnnui:RadDatePicker ID="txtDate" Width="250px" runat="server">
                    <calendar id="Calendar1" runat="server" enablekeyboardnavigation="true">
                </calendar>
                    <dateinput tooltip="Choose Date"></dateinput>
                </dnnui:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                    ErrorMessage="Date is required." CssClass="NormalRed" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; width: 200px;">Upload File</td>
            <td>
                <dnn:FilePickerUploader ID="fileuploader" FileFilter="pdf" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">Description:
            <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <dnn:TextEditor runat="server" id="txtDescription" Height="500px" Width="1000px"></dnn:TextEditor>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:LinkButton ID="btnSubmit" runat="server"
                    OnClick="btnSubmit_Click" Text="Save" CssClass="dnnPrimaryAction" />
                <asp:LinkButton ID="btncancel" runat="server"
                    OnClick="btnCancel_Click" Text="Cancel" CssClass="dnnSecondaryAction" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    function dateSelected(sender, args) {
        if (args.get_oldDate() == null) {
            args.set_cancel(true);
        }
    }
</script>

