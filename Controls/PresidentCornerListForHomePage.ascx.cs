﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.PresidentCorner.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.PresidentCorner.Controls
{
    public partial class PresidentCornerListForHomePage : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var tc = new PresidentCornerController();
                rptItemList2.DataSource = tc.GetTop3Corner(PortalId);
                rptItemList2.DataBind();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        protected void lnkProject_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/president-corner");
        }
    }
}