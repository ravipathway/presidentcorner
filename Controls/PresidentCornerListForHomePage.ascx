﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PresidentCornerListForHomePage.ascx.cs" Inherits="NRNA.Modules.PresidentCorner.Controls.PresidentCornerListForHomePage" %>

<asp:Repeater ID="rptItemList2" runat="server">
    <HeaderTemplate>
        <ul class="list-a">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <asp:HyperLink runat="server" ID="lnkTitle" Target="_blank"
                NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Attachment").ToString() != "-1" ?
                              GetImageFromImageID(DataBinder.Eval(Container.DataItem,"Attachment").ToString()): "~/president-corner/pcid/" + DataBinder.Eval(Container.DataItem,"CornerId").ToString() %>'
                Text='<%# DataBinder.Eval(Container.DataItem, "CornerTitle").ToString() %>'>
            </asp:HyperLink>
        </li>
    </ItemTemplate>

    <FooterTemplate>
        </ul>
                   
    </FooterTemplate>
</asp:Repeater>
<div class=" mrg-sm-l mrg-xs-b">
    <asp:LinkButton runat="server" ID="lnkContent" OnClick="lnkProject_Click" Text="View all"></asp:LinkButton>
</div>
