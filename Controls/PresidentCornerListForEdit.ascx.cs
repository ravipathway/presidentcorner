﻿using DotNetNuke.Services.Exceptions;
using DotNetNuke.UI.Utilities;
using NRNA.Modules.PresidentCorner.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.PresidentCorner.Controls
{
    public partial class PresidentCornerListForEdit : PresidentCornerModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bindGV();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        private void bindGV()
        {
            var tc = new PresidentCornerController();
            gv.DataSource = tc.GetPresidentCorner();
            gv.DataBind();
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lnkDelete = e.Row.FindControl("lnkDelete") as LinkButton;
                var t = (NRNA.Modules.PresidentCorner.Components.PresidentCorner)e.Row.DataItem;

                lnkDelete.CommandArgument = t.CornerId.ToString();
                lnkDelete.Enabled = lnkDelete.Visible = true;
                ClientAPI.AddButtonConfirm(lnkDelete, "Are you sure you want to delete?");
            }
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                var tc = new PresidentCornerController();
                tc.DeletePresidentCorner(Convert.ToInt32(e.CommandArgument), PortalId);
            }
        }

        protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            bindGV();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = e.NewPageIndex;
            bindGV();
        }
    }
}