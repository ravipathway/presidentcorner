﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PresidentCornerList.ascx.cs" Inherits="NRNA.Modules.PresidentCorner.Controls.PresidentCornerList" %>
<h2>President Corner</h2>

<asp:Repeater ID="rptItemList2" runat="server">
    <HeaderTemplate>
        <ul class="aside-link-box">
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <h4>
                <asp:HyperLink runat="server" ID="lnkTitle" Target="_blank"
                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Attachment").ToString() != "-1" ?
                              GetImageFromImageID(DataBinder.Eval(Container.DataItem,"Attachment").ToString()): "~/president-corner/pcid/" + DataBinder.Eval(Container.DataItem,"CornerId").ToString() %>'
                    Text='<%# DataBinder.Eval(Container.DataItem, "CornerTitle").ToString() %>'>
                </asp:HyperLink>
            </h4>
        </li>

    </ItemTemplate>

    <FooterTemplate>
        </ul>
                   
    </FooterTemplate>
</asp:Repeater>

<asp:Literal ID="litPaging" runat="server"></asp:Literal>